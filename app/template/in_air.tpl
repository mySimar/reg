{{#each inair_data}}
{{#if is_aircraft3}}
<div class="item">
                    <div class="airplane-info">
                        <div class="info-mark airplane-m">
                            <p class="type">{{planer_info.name}}</p>
                            <p class="model">{{planer_info.description}}</p>
                        </div>
                        <div class="info-cadet airplane-c">
                            <span class="cadet-number">{{planer_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{planer_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">5 мин.</span> в воздухе</p>
                            </div>
                        </div>
                        <div class="info-instructor airplane-instr">
                            <span class="instructor-number">{{instructor_info.pilot_code}}</span>
                            <div>
                                <p class="instructor-name">{{instructor_info.name}}</p>
                                <p class="instructor-exercises">Упражнение №<span class="exercises-number">7</span><span class="exercises-type">, Круги</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="item-btn">
                        <button class="btn btn-separate">Отцепка</button>
                    </div>
                    <div class="sailplane-info">
                        <div class="info-mark sailplane-m">
                            <p class="type">{{plane_info.name}}</p>
                            <p class="model">{{plane_info.description}}</p>
                        </div>
                        <div class="info-cadet sailplane-c">
                            <span class="cadet-number">{{plane_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{plane_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">8 мин.</span> в воздухе</p>
                            </div>
                        </div>
                    </div>
                    <span class="ion-edit"></span>
                </div>
{{/if}}
{{#if is_detouch}}
<div class="item">
                    <div class="airplane-info">
                        <div class="info-mark airplane-m">
                            <p class="type">{{planer_info.name}}</p>
                            <p class="model">{{planer_info.description}}</p>
                        </div>
                        <div class="info-cadet airplane-c">
                            <span class="cadet-number">{{planer_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{planer_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">5 мин.</span> в воздухе</p>
                            </div>
                        </div>
                        <div class="info-instructor airplane-instr">
                            <span class="instructor-number">{{instructor_info.pilot_code}}</span>
                            <div>
                                <p class="instructor-name">{{instructor_info.name}}</p>
                                <p class="instructor-exercises">Упражнение №<span class="exercises-number">7</span><span class="exercises-type">, Круги</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="item-btn btn-landing">
                        <button class="btn" data-id={{id}}>Посадка</button>
                    </div>
                    <span class="ion-edit"></span>
                </div>
                <div class="item">
                    <div class="sailplane-info">
                        <div class="info-mark sailplane-m">
                            <p class="type">{{plane_info.name}}</p>
                            <p class="model">{{plane_info.description}}</p>
                        </div>
                        <div class="info-cadet sailplane-c">
                            <span class="cadet-number">{{plane_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{plane_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">8 мин.</span> в воздухе</p>
                            </div>
                        </div>
                    </div>
                    <div class="item-btn btn-landing">
                        <button class="btn" data-id={{id}}>Посадка</button>
                    </div>
                    <span class="ion-edit"></span>
                </div>
{{/if}}
{{#if is_planer}}
<div class="item">
                    <div class="airplane-info">
                        <div class="info-mark airplane-m">
                            <p class="type">{{planer_info.name}}</p>
                            <p class="model">{{planer_info.description}}</p>
                        </div>
                        <div class="info-cadet airplane-c">
                            <span class="cadet-number">{{planer_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{planer_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">5 мин.</span> в воздухе</p>
                            </div>
                        </div>
                        <div class="info-instructor airplane-instr">
                            <span class="instructor-number">{{instructor_info.pilot_code}}</span>
                            <div>
                                <p class="instructor-name">{{instructor_info.name}}</p>
                                <p class="instructor-exercises">Упражнение №<span class="exercises-number">7</span><span class="exercises-type">, Круги</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="item-btn btn-landing">
                        <button class="btn" data-id={{id}}>Посадка</button>
                    </div>
                    <span class="ion-edit"></span>
                </div>
{{/if}}
{{#if is_plane}}
<div class="item">
                    <div class="sailplane-info">
                        <div class="info-mark sailplane-m">
                            <p class="type">{{plane_info.name}}</p>
                            <p class="model">{{plane_info.description}}</p>
                        </div>
                        <div class="info-cadet sailplane-c">
                            <span class="cadet-number">{{plane_pilot_info.pilot_code}}</span>
                            <div>
                                <p class="cadet-name">{{plane_pilot_info.name}}</p>
                                <p class="cadet-air-time"><span class="time">8 мин.</span> в воздухе</p>
                            </div>
                        </div>
                    </div>
                    <div class="item-btn btn-landing">
                        <button class="btn" data-id={{id}}>Посадка</button>
                    </div>
                    <span class="ion-edit"></span>
                </div>
{{/if}}

{{/each}}
