require.config({
	paths: {
		"jquery" : "libs/jquery",
		"handlebars" : "libs/handlebars",
		//text plugin
		text:'libs/text',
		tpl: 'libs/tpl'
	},
	urlArgs: "v=" + (new Date()).getTime()
});
require([
	"jquery","handlebars", "modules/events","modules/chips", "modules/in_air", 
	"modules/pilots", "modules/planer", "modules/plane"], 
	function($, Handlebars, event, chips, in_air, pilot, planer, plane) {
		pilot.getDataPilots().then(function() {
			pilot.renderPilotsPlaner();
			pilot.renderInstructor();
			pilot.renderPilotsPlane();
		});
		planer.getDataPlaner().then(function() {
			planer.renderModelPlaner();
		});
		plane.getDataPlane().then(function() {
			plane.renderModelPlane();
		});
		chips.getPlanerChips().then(function() {
			chips.getChips();
			chips.renderPlanerChips();
		}, function() {
			console.log('error find ajax planer chips');
		});
		in_air.getFly().then(function() {
			in_air.getStatus();
			in_air.renderFly();
			console.log(in_air.addStatus(407));
			console.log(in_air.getStatusById(407))
		});

		// Events chips
		$(document).on('click', '.sailplane-model', function(e) {
			e.stopPropagation();
			var idPlaner = $(this).text();
			var retryId = planer.getPlanerById(idPlaner);
			console.log(retryId[0].name);
		});
});