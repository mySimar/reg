$(document).ready(function() {
    // Event 
    $('#planer, #plane, #planer_pilot, #plane_pilot, #instructor, #exercise').on('input', searchItems);
    $('.ion-close-round').click(deleteVal);
});

$(document).on('click', '.autocomplete>p', function () {
    var textEl = $(this).text();
    var el =  $(this).parent().prev();
    el.val(textEl);
    if( $(this).attr('data-is_double') == 'false' ) {
        $('#instructor').prop('disabled', true);
    } else {
        $('#instructor').removeProp('disabled');
    }
});

// Planer number ajax
function getPlaners() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/planer/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getPlaners().then(function(data) {
    createRenderPlaner(data)
});

function createRenderPlaner(msg) {
    var $str = isNotFly(msg);
    var planerNumDiv = document.getElementsByClassName('autocomplete')[0];
    for (let i of $str) {
        var p = document.createElement('p');
        planerNumDiv.appendChild(p);
        p.innerHTML=i.name;
        if (i.is_double) {
            p.setAttribute('data-is_double', true);
        } else {
            p.setAttribute('data-is_double', false);
        }
        p.setAttribute('data-id', i.id);
    }
    /*for (var i=0, str=$str.length; i<str; i++) {
        var p = document.createElement('p');
        planerNumDiv.appendChild(p);
        p.innerHTML=$str[i].name;
        if ($str[i].is_double) {
            p.setAttribute('data-is_double', true);
        } else {
            p.setAttribute('data-is_double', false);
        }
        p.setAttribute('data-id', $str[i].id);
    }*/
}

// Plane number ajax
function getPlane() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/plane/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getPlane().then(function(data) {
    createRenderPlane(data)
});

function createRenderPlane(msg) {
    var $str = isNotFly(msg);
     var planeNumDiv = document.getElementsByClassName('autocomplete')[4];
    for (var i=0, str=$str.length; i<str; i++) {
        var p = document.createElement('p');
        planeNumDiv.appendChild(p);
        p.innerHTML=$str[i].name;
        p.setAttribute('data-id', $str[i].id);
    }

}

// Pilot ajax
function getPilots() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/pilots/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getPilots().then(function(data) {
    createRenderPilots(data)
});

function createRenderPilots(msg) {
    // Planer pilot ajax
    var $str = isNotFly(msg);
    var planerPilDiv = document.getElementsByClassName('autocomplete')[1];
    for(let i of $str) {
        var p = document.createElement('p');
        planerPilDiv.appendChild(p);
        p.innerHTML=i.pilot_code+' '+i.name;
    }
    /*for (var i=0, str=$str.length; i<str; i++) {
        var p = document.createElement('p');
        planerPilDiv.appendChild(p);
        p.innerHTML=$str[i].pilot_code+' '+$str[i].name;
    }*/

    // Plane pilot ajax
    var pilotPlane = msg.filter(function(item) {
        return !item.in_fly && item.is_pilot_of_plane;
    });
    var pilPlaneName = pilotPlane.map(function(itemId) {
        return itemId.pilot_code+' '+itemId.name;
    });
    var planePilDiv = document.getElementsByClassName('autocomplete')[5];
    for(let i of pilPlaneName) {
        var p = document.createElement('p');
        planePilDiv.appendChild(p);
        p.innerHTML=i;
    }
    /*for (var i=0, str=pilPlaneName.length; i<str; i++) {
        var p = document.createElement('p');
        planePilDiv.appendChild(p);
        p.innerHTML=pilPlaneName[i];
    }*/

    // Planer instructor ajax
    var instPlaner = msg.filter(function(item) {
        return !item.in_fly && item.is_instructor;
    });
    var instName = instPlaner.map(function(itemId) {
        return itemId.pilot_code+' '+itemId.name;
    });
    var planerInstDiv = document.getElementsByClassName('autocomplete')[2];
    for(let i of pilPlaneName) {
        var p = document.createElement('p');
        planerInstDiv.appendChild(p);
        p.innerHTML=i;
    }
    /*for (var i=0, str=instName.length; i<str; i++) {
        var p = document.createElement('p');
        planerInstDiv.appendChild(p);
        p.innerHTML=instName[i];
    }*/
}


// Exercise planer
function getExercise() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/exercise/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getExercise().then(function(data) {
    createRenderExercise(data)
});

function createRenderExercise(msg) {
    var exercise = msg.map(function(item) {
        return item.name+' '+item.description;
    });
    var exeDiv = document.getElementsByClassName('autocomplete')[3];
    for(let i of exercise) {
        var p = document.createElement('p');
        exeDiv.appendChild(p);
        p.innerHTML=i;
    }
    /*for (var i=0, exerciseDiv=exercise.length; i<exerciseDiv; i++) {
        var p = document.createElement('p');
        exeDiv.appendChild(p);
        p.innerHTML=exercise[i];
    }*/
}

// Event
function searchItems() {
    var items = $(this).next().children(); //autocomplite p
    var findDivErrorP = $(this).parent().find('.error-empty-search').children(); // error p
    for(var i = 0, max = items.length; i < max; i++) {
        var itemIndex = items[i].innerText.toLowerCase().indexOf(this.value.toLowerCase());
        if( this.value.length > 0 ) {
            if( itemIndex > -1 ) {
                items[i].style.display="block";
                findDivErrorP.css('display', 'none');
            } else {
                items[i].style.display="none";
                findDivErrorP.css('display', 'block');
            }
        } else {
            items[i].style.display="none";
            findDivErrorP.css('display', 'none');
        }
    }
}

// Event delete value
function deleteVal() {
    var input = $(this).parent().find('input');
    input.val('');
}

function isNotFly(array) {
 return array.filter(function(item) {
    return !item.in_fly;
 });
}