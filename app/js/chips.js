$(document).ready(function() {

});

// Event
$(document).on('click','.airplane-model', function(e) {
    //e.stopPropagation();
    $('.autocomplete>p[data-id="'+ $(this).text()  +'"]').click();
});
$(document).on('click','.sailplane-model', function(e) {
    $('.autocomplete>p[data-id="'+ $(this).text()  +'"]').click();
});
$(document).on('click', '.close-chips', function (e) {
    e.stopPropagation();
    var chips = $(this).parent().attr('data-planer-id');
    localStorage[chips] = true;
    $(this).parent().remove();
});

// Planer chip ajax
function getPlanerChips() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/chips/planers/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getPlanerChips().then(function(data) {
    planerChipsRender(data)
});

function planerChipsRender(msg) {
   //debugger
    var templateScript = $('#book').html();
    var template = Handlebars.compile(templateScript);

    /*var $str = msg;
    var planerChip = '';
    for(i=0;i<$str.length;i++) {
        if (!localStorage[$str[i].planer_id]) {
            planerChip += '<span data-planer-id="'+$str[i].planer_id+'" class="sailplane-model">'+$str[i].planer_id+'<i class="ion-close-round close-chips"></i></span>';
        }
    }*/

    $('.sailplane-status').html(template({chips_data:msg}));
}

// Plane chip ajax
function getPlaneChips() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/chips/planes/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getPlaneChips().then(function(data) {
    planeChipsRender(data)
});

function planeChipsRender(msg) {
    var $str = msg;
    var planeChip = '';
    for(i=0;i<$str.length;i++) {
        var classstatus  = '';
        if($str[i].is_engine_work) {
            classstatus  += ' ion-stop'
        }
        if (!localStorage[$str[i].plane_id]) {
            planeChip += '<span data-planer-id="'+$str[i].plane_id+'" data-engine="'+ $str[i].is_engine_work +'" class="airplane-model">'+$str[i].plane_id+'<i class="ion-close-round '+  classstatus +' close-chips"></i></span>';
        }
    }
    $('.airplane-status').html(planeChip);
}