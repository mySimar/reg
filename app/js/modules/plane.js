define ([], function () {
    var plane = [];
    function getDataPlane() {
        return new Promise(function(resolve, reject) {
            $.ajax({
                type: "get",
                url: "https://flytimeserver.herokuapp.com/plane/",
                success: function(msg) {
                    resolve(msg);
                    plane = msg;
                },
                error: function() {
                    reject('error get');
                }
            });
        });
    }
    function isNotFly() {
        var notFly = plane.filter(function(item) {
            return !item.in_fly;
        });
        return notFly
    }
    function renderModelPlane() {
        var model = isNotFly();
        require(['handlebars','text!../template/plane.tpl'],function(Handlebars,tpl) {  //view
            var template = Handlebars.compile(tpl);
            var html =   template({pilot_data:model});
            $('.autocomplete').eq(4).html(html);
        })
    }
	return {
        'getDataPlane': getDataPlane,
        'renderModelPlane': renderModelPlane
	}
});