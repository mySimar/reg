define(["libs/polifil"], function() {
	// Planer chip ajax
	var planerChips = [];
	function getPlanerChips() {
		return new Promise(function(resolve, reject) {
			$.ajax({
				type: "get",
				url: "https://flytimeserver.herokuapp.com/chips/planers/",
				success: function(msg) {
					resolve(msg);
					planerChips = msg;
				},
				error: function() {
					reject('error get');
					setTimeout(function() {
						getPlanerChips();
					}, 5000)
				}
			});
		});
	}
	function getChips() {
		//console.log(planerChips)

	}
	function renderPlanerChips() {
		require(['handlebars', 'text!../template/chips.tpl'], function(Handlebars, tpl) {  //view
            var template = Handlebars.compile(tpl);
            var html = template({chip_data:planerChips});
            $('.sailplane-status').html(html);
        });
	}
	$(document).on('click', '.ion-close-round', deleteChips)
	function deleteChips(e) {
		e.preventDefault();
		e.stopPropagation();
		var idChips = $(this).parent().text()
		var searchIndex = planerChips.findIndex(function(item) {
			return item.planer_id == idChips
		});
		planerChips.splice(searchIndex, 1);
		renderPlanerChips();
	}
	return {
		'getPlanerChips': getPlanerChips,
		'getChips': getChips,
		'renderPlanerChips': renderPlanerChips
	}
})