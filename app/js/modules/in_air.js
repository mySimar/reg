define([], function() {
    var air = [];
    var airclone = [];
    function getFly() {
        return new Promise(function(resolve, reject) {
            $.ajax({
                type: "get",
                url: "https://flytimeserver.herokuapp.com/fly/",
                success: function(msg) {
                    resolve(msg);
                    air = msg;
                },
                error: function() {
                    reject('error get');
                }
            });
        });
    }
    function getStatus() {
        for (var i = 0, max = air.length; i < max; i++) {
            if (jQuery.inArray(5, air[i].statuses) > -1) {
                console.log("літак");
                air[i].is_plane = true;
                airclone.push(air[i])
            } else if (jQuery.inArray(4, air[i].statuses) > -1) {
                console.log("планер");
                air[i].is_planer = true;
                airclone.push(air[i])
            } else if (jQuery.inArray(2, air[i].statuses) > -1) {
                console.log("розєднанний аеропоїзд");
                air[i].is_detouch = true;
                airclone.push(air[i])
            } else if (jQuery.inArray(1, air[i].statuses) > -1) {
                console.log("аеропоїзд");
                air[i].is_aircraft = true;
                airclone.push(air[i])
            } else {
                console.log("невідомий запит");
            }
        }
    }
    function getAirCraft() {
        return airclone.filter(function(item) {
            return item.is_aircraft
        })
    }
    function getDetouch() {
        return airclone.filter(function(item) {
            return item.is_detouch
        })
    }
    function getPlanerAir() {
        return airclone.filter(function(item) {
            return item.is_planer
        })
    }
    function getPlaneAir() {
        return airclone.filter(function(item) {
            return item.is_plane
        })
    }
    function getInfoById(index) {
        return airclone.filter(function(item) {
            return item.id == index
        })
    }
    function renderFly() {
        require(['handlebars', 'text!../template/in_air.tpl'], function(Handlebars, tpl) {
            var template = Handlebars.compile(tpl);
            var html = template({inair_data:airclone});
            $('.in-air-wrap').html(html);
        });
    }
    // Event button
        $(document).on('click', '.btn-landing>.btn', function(e) {
            var btnId = $(this).attr('data-id');
            var getId = getInfoById(btnId);
            console.log(getId)
            airclone.splice(getId, 1);
            renderFly();
        })
    function getStatusById(id) {
        return getInfoById(id).map(function(item) {
            return item.statuses;
        })
    }
    function addStatus(id) {
        var x = getStatusById(id);
        return x[0]
    } 
    return {
        'getFly': getFly,
        'getStatus': getStatus,
        'getAirCraft': getAirCraft,
        'getDetouch': getDetouch,
        'getPlanerAir': getPlanerAir,
        'getPlaneAir': getPlaneAir,
        'getInfoById': getInfoById,
        'renderFly': renderFly,
        'getStatusById': getStatusById,
        'addStatus': addStatus
    }
})