define ([], function () {

	var pilots = [];


	function getDataPilots() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/pilots/",
            success: function(msg) {
                resolve(msg);
                pilots = msg;
            },
            error: function() {
                reject('error get');
            }
        });
    });
    }
    function getPilots() {
    	console.log(pilots.length)
    }
    function clearPilots() {
    	pilots.length = 0;
    }
    function isNotFly() {
    	var notFly = pilots.filter(function(item) {
    		return !item.in_fly;
    	})
    	return notFly
    }
    function getPilotsPlane() {
    	return isNotFly().filter(function(item) {
    		return item.is_pilot_of_plane 
    	})
    }
    function getInstructors() {
    	return pilots.filter(function(item) {
    		return item.is_instructor
    	})
    }
    function renderPilotsPlaner() {
    	var pil = isNotFly(); //model
        require(['handlebars','text!../template/pilots.tpl'],function(Handlebars,tpl) {  //view
            var template = Handlebars.compile(tpl);
            var html =   template({pilot_data:pil});
            $('.autocomplete').eq(1).html(html);
        })


    }
    function renderInstructor() {
        var instr = getInstructors();
        require(['handlebars','text!../template/pilots.tpl'],function(Handlebars,tpl) {
            var template = Handlebars.compile(tpl);
            var html =   template({pilot_data:instr});
            $('.autocomplete').eq(2).html(html);
        })
    }
    function renderPilotsPlane() {
        var pil = getPilotsPlane(); //model
        require(['handlebars','text!../template/pilots.tpl'],function(Handlebars,tpl) {  //view
            var template = Handlebars.compile(tpl);
            var html =   template({pilot_data:pil});
            $('.autocomplete').eq(5).html(html);
        })
    }
	return {
		'getDataPilots': getDataPilots,
		//'getPilots': getPilots,
		//'clearPilots': clearPilots,
		//'isNotFly': isNotFly,
		'renderPilotsPlaner': renderPilotsPlaner,
        'renderInstructor': renderInstructor,
        'renderPilotsPlane': renderPilotsPlane
	}

});