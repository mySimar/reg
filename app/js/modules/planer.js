define ([], function () {
	var planer = [];
	function getDataPlaner() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/planer/",
            success: function(msg) {
                resolve(msg);
                planer = msg;
            },
            error: function() {
                reject('error get');
            }
        });
    });
    }
    function isNotFly() {
    	var notFly = planer.filter(function(item) {
    		return !item.in_fly;
    	});
    	return notFly
    }
    function getPlanerById(index) {
        return planer.filter(function(item) {
            return item.id == index
        })
    }
    function renderModelPlaner() {
    	var pil = isNotFly(); //model
        require(['handlebars','text!../template/planer.tpl'],function(Handlebars,tpl) {  //view
            var template = Handlebars.compile(tpl);
            var html =   template({pilot_data:pil});
            $('.autocomplete').eq(0).html(html);
        })
    }
	return {
		'getDataPlaner': getDataPlaner,
		'renderModelPlaner': renderModelPlaner,
        'getPlanerById': getPlanerById
	}

});