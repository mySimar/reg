console.log('events file')
define (["jquery"], function () {
var selector = '#planer, #planer_pilot, #instructor, #plane, #plane_pilot'
$(document).ready(function() {
	$('.ion-close-round').click(deleteVal);
})
$(document).on('input', selector, findModelPlaner);
function findModelPlaner() {
    var items = $(this).next().children(); //autocomplite p
    var findDivErrorP = $(this).parent().find('.error-empty-search').children(); // error p
    var selcted = false;
    for(var i = 0, max = items.length; i < max; i++) {
       var itemIndex = items[i].innerText.toLowerCase().indexOf(this.value.toLowerCase());
        if( this.value.length > 0 ) {
            if( itemIndex > -1 ) {
            	selcted=true
                items[i].style.display="block";
            } else {
                items[i].style.display="none";
            }
        } else {
            items[i].style.display="none";
        }
    }
    if(selcted) {
        	findDivErrorP.css('display', 'none');
        } else {
        	if(!this.value.length) return
        	findDivErrorP.css('display', 'block');
        }
}
$(document).on('click', '.autocomplete>p', selectName);
function selectName() {

	$(this).parent().parent().find('input').val($(this).text());
}
// Event delete value
function deleteVal() {
    var input = $(this).parent().find('input');
    input.val('');
    $('.autocomplete>p').hide();
    $('.error-empty-search>p').hide();
}

})
